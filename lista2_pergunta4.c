#include <stdio.h>

int main (){

	int ano; 
	
	printf("digite o ano: ");
	scanf("%d", &ano);
	
	if (ano % 100 != 0 && ano % 4 == 0 || ano % 400 == 0){
		printf("***Ano Bissexto***");
	}
	else{
		printf("***ano nao bissexto***");
	}

	return 0 ;
}
